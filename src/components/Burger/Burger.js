import React from 'react';

import './Burger.css';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'

const burger = (props) => {
    let transformedIngredients = Object.keys(props.ingredients) /*transform object in array on keys value*/
        .map(ingredientKey => {
            return [...Array(props.ingredients[ingredientKey])] // create array witch length is same as amount of given ingredient in object rec trough props (cheese:2,array will have 2 empty spaces, but it length will bi 2)
                .map((_, i) => {
                    return <BurgerIngredient key={ingredientKey + i} type={ingredientKey} />
                }); //pretvorili objekat sastojaka koji je prosledjen kroz propse u niz ciji su elementi stringovi propretija objekta ingredients(cjeese, meat itd.), a nakon toga taj niz mapiramo i dobijamo niz ciji su elementi nizovi cija je duzina jednaka vrednosti tog propretija u objektu ingredients(tu nam nije bitno koji  je sastojak u pitanju i zato je kod map funkcije underline kao prvi parametar(valjda znaci da nema naziv)). na kraju se vraca za svaki sastojak onoliko <BurgerIngredient/>  komponenti kolik aj e vrednost svojstva tog sastojak u objektu ingredients. 
        }).reduce((arr, el) => { 
            return arr.concat(el) }, []); //Sa reduce smo niz nizova pretvorili u niz koji mozemo da preveravamo da li je prazan ili nije
    if (transformedIngredients.length === 0) {
        transformedIngredients = <p>Please start adding ingredients</p>
    }
    return (
        <div className='burger'>
            <BurgerIngredient type="bread-top" />
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
};

export default burger