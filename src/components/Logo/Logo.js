import React from 'react';

import img from '../../assets/images/burger-logo.png';
import './Logo.css'

const logo = (props) =>  (
       <div className='logo'>
           <img src={img} alt='logo' />
       </div>
    );


export default logo;