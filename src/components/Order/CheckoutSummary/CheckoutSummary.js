import React from 'react';

import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';
import './CheckoutSummary.css'

const checkoutSummary = (props) => (

    <div className='checkoutSummary'>
        <h1>We hope it taste well!</h1>
        <div style={{width: '100%', margin: 'auto'}}>
            <Burger ingredients={props.ingredients} />
        </div>
        <Button
            btnType='danger'
            clicked={props.onCheckoutCancelled}>CANCEL</Button>
        <Button
            btnType='success'
            clicked={props.onCheckoutContinued}>CONTINUE</Button>
    </div>
)

export default checkoutSummary;