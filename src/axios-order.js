import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://my-burger-learning-redux.firebaseio.com/',
});


export default instance