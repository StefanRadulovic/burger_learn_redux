import React, { Component } from 'react';
import axios from '../../../axios-order';

import Spinner from '../../../components/UI/Spinner/Spinner';
import Button from '../../../components/UI/Button/Button';
import './ContactData.css'


class ContactData extends Component {
    state = {
        name: '',
        email: '',
        address: {
            street: '',
            postalCode: ''
        }

    }
    orderHandler = (event) => {
        event.preventDefault();
        this.setState({
            loading: true
        })
        this.setState({
            loading: true
        })
        const order = {
            ingredients: this.props.ingredients,
            price: this.props.price,
            customer: {
                name: 'Stefan',
                address: {
                    street: 'Nehruova',
                    zipCode: '11000',
                    country: 'Serbia'
                },
                email: 'test@test.com'
            },
            deliveryMethod: 'fastest'
        }
        axios.post('/orders.json', order)
            .then(response =>{
                this.setState({
                    loading: false,
                });
                this.props.history.push('/');
            })
            .catch(error => {
                this.setState({
                    loading: false,
                })
            })
    }
    render() {
        let form = (
            <form>
                <input className='input' type='email' name='email' placeholder='Your Email' />
                <input className='input' type='text' name='name' placeholder='Your Name' />
                <input className='input' type='text' name='street' placeholder='Street' />
                <input className='input' type='text' name='postal' placeholder='Postal Code' />
                <Button btnType='success' clicked={this.orderHandler}> ORDER</Button>
            </form>
        );
        if (this.state.loading) {
            form = <Spinner />
        }
        return (
            <div className='contactData'>
                <h4>Enter your contact data</h4>
                {form}
            </div>
        )
    }
}

export default ContactData;

